﻿Đêm Giới Nghiêm - Hồi Ức Về Cuộc Sống, Tình Yêu Và Chiến Tranh Ở Kashmir
Mã sách: #6005

Tác giả: Basharat Peer
Giá bìa: 110,000 VNĐ
Số trang: 397
Ngày Xuất Bản: 03-2012
Nhà Xuất Bản: NXB Trẻ
Nội dung:
Nằm giữa Ấn độ và Pakistan, Kashmir là một trong những nơi đẹp nhất trên Trái đất. Tuy nhiên, cuộc sống ở đây lại là một thực tế hoàn toàn khác. Kể từ khi phong trào ly khai bùng phát vào năm 1989, hàng vạn người đã bị giết trong cuộc chiến giành quyền kiểm soát khu vực này.

Basharat Peer đã tái dựng hiện thực kinh hoàng của vùng đất này trong cuộc xung đột. Đó là câu chuyện về bước đầu gia nhập trại huấn luyện vũ trang ở Pakistan của một thanh niên, về một người mẹ chứng kiến con trai mình bị toán quân Ấn Độ buộc ôm một trái bom, về một nhà thơ đi tìm đức tin tôn giáo khi toàn bộ gia đình ông bị giết, về các chính khách sống trong các phòng tra tấn được tân trang, những ngôi làng bị xé toang bởi mìn và các đền thờ Sufi cổ kính bị đánh bom tan hoang…
 
Tác phẩm là một thiên phóng sự của một cây bút trẻ can đảm, người mang cả một thế giới mới – sống động và chân thực – đến với độc giả.