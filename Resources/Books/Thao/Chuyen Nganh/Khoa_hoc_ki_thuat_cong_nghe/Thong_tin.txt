﻿Khoa học Kỹ thuật Công nghệ Cao su thiên nhiên
Tác giả: KS. Nguyễn Hữu Trí 
Nhà phát hành: Chưa có
Tập sách "Khoa học kỹ thuật Công nghệ cao su thiên nhiên" được biên soạn hết sức công phu và nghiêm túc dựa trên nghiên cứu, tham khảo và quá trình kinh nghiệm sản xuất. Sách gồm 18 chương, cuốn sách nêu khái quát về lịch sử phát hiện và phát triển cây cao su, các họ cây cao su, những kinh nghiệm trồng và chǎm sóc cây cao su v.v... .Sách cũng mang đến cho người đọc phổ thông những kiến thức hữu ích về sản phẩm mà mình đang sử dụng.