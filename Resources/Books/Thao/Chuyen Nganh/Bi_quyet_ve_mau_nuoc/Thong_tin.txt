﻿Bí Quyết Vẽ Màu Nước
Tác giả: Huỳnh Phạm Hương Trang 
Nhà xuất bản: Nxb Mỹ thuật
Nhà phát hành: Văn Lang
Nội dung:
Cuốn sách này được biên soạn trên nền giấy trắng láng, nhiều hình ảnh màu minh họa rất đẹp, hướng dẫn bạn từ lúc chuẩn bị vật liệu đến cách cầm cọ và vẽ. Bí Quyết Vẽ Màu Nước là cuốn sách rất bổ ích và cần thiết đối với các bạn sinh viên ngành hội họa.