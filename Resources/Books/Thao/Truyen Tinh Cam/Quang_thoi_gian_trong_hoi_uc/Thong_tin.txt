﻿Quãng Thời Gian Trong Hồi Ức
Mã sách: #8101

Tác giả: Diệp Tử
Giá bìa: 129,000 VNĐ
Số trang:
Ngày Xuất Bản: 06-2014
Nhà Xuất Bản: Văn học - Văn Việt
Nội dung:
Những gì đã từng có, xin đừng vội lãng quên. Những gì không có được, lại càng phải trân trọng. Những gì thuộc về mình, đừng dễ dàng từ bỏ. Những gì đã mất đi, hãy giữ làm hồi ức.　

… Có lẽ đã từng lướt qua nhau trong biển người mênh mông, lúc ngoảnh đầu nhìn lại sẽ có cảm giác quen thuộc. Cũng có thể một phía vẫn còn độc thân, ánh mắt đăm đắm nhìn dán vào người kia. Cũng không loại trừ cả hai vẫn một mình, nhìn nhau nở nụ cười, sau đó hoặc sẽ tiếp tục duyên phận trước kia, hoặc sẽ vì hàng vạn nguyên do mà lại bỏ qua nhau lần nữa. Đương nhiên có khả năng nhất là anh đã có vợ, em đã có chồng, mỗi người đều khoác tay nửa kia của mình, gặp lại nhau khi đang tản bộ trong công viên, chuyện cũ ùa về, nhìn nhau mỉm cười rồi tiếp tục đi lướt qua nhau…