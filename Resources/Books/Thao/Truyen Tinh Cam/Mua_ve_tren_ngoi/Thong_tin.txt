﻿Mùa Về Trên Ngói
Mã sách: #8046

Tác giả: Hồ Minh Thông
Giá bìa: 76,000 VNĐ
Số trang: 273
Ngày Xuất Bản: 04-2014
Nhà Xuất Bản: Văn học - Skybooks
Nội dung:
Trẻ con rồi cũng lớn lên. Đứa nào đứa nấy chênh vênh những buồn… Nỗi buồn chênh vênh nghiêng ngả khi nhớ về một mảng kí ức xa xôi, nơi có tình yêu đầu vụng dại, có con diều biếc chao nghiêng, có bóng mẹ gầy đợi chờ trước ngõ, có tiếng mèo hoang xa vắng, và tâm sự của loài rêu hao gầy... Đó là nỗi buồn khi nhớ về tuổi thơ…

Hẳn rồi khi lớn lên, chúng ta đã phải vứt bỏ đi rất nhiều thứ mang tên kỉ niệm. Không phải cố tình, cũng không chỉ vô ý, chỉ là trong lúc mải lớn lên, nhiều thứ thay đổi quá, nhiều thứ mất đi quá. Bỗng muốn tìm về… cũng thấy xa xăm. Tất cả chỉ còn mập mờ trong kí ức, lâu lâu lại vọng về, nhức nhối con tim. Và rồi, như một kiểu tự sự thân quen.

Mùa về trên ngói dắt ta ngược về tuổi thơ, ngắm con đò mải miết no trăng, ngửi thấy mùi ngai ngái của những gốc rạ sau mưa, lặng lẽ bên một quán cóc mùa đông heo hút, hát những khúc đồng dao, nếm vị ngọt của chiếc kẹo bột… hay chơi vơi giữa phố chuyển mùa. Tất cả những điều đó, đã từng rất thân quen. Nhưng rồi cứ lớn lên, con người ta quên mất, quên cả lối tìm về.

Cuốn sách chỉ xoay vần quanh những câu chuyện cũ mà tuổi thơ ai rồi cũng đã một lần nếm qua. Một cuộc sống bình yên êm ả lấp sau lũy tre làng… Vì ai mà chẳng có quê hương. Nhưng rồi người ta cũng thấy lạ quá, khi giữa những bộn bề này, đâu còn thấy bóng con chim sẻ đồng lạc bạn, đâu còn nghe được lời rêu hao gầy, đâu còn nhớ đến khúc đồng dao, đôi khi còn hiếm hoi cả những bức thư tay…

Cứ coi như là, Skybooks gửi đến bạn Mùa về trên ngói, như tấm vé lội dòng thời gian, tìm về với một thời đã cũ, để nhớ, để yêu, để bớt chênh vênh giữa vòng xoáy hiện tại… để nghe mùa chuyển mình trên ngói.